#!/bin/bash

# Função para exibir a saída do comando "ssh"
function ssh_error {
    if [ $? -ne 0 ]; then
        yad --title "Erro na Transferência" --image gtk-dialog-error --text "Houve um erro na transferência do arquivo. Verifique se as informações fornecidas estão corretas e tente novamente." --center
    fi
}

# Função para obter informações do usuário
function obter_informacoes {

    DADOS=$(
    yad --title="TUXSCP" --center                   				\
        --image tuxscp_icon.png                     				\
        --width=500 --height=600                    				\
        --form                                      				\
        --field="Nome do usuário: " ""                				\
        --field="IP do servidor: " ""              				\
        --field="Senha do servidor ":H ""   					\
        --field="Digite o diretorio de destino no servidor: " ""		\
        --field="Digite o caminho do arquivo que deseja transferir: " ""	\

          )
    # Extraindo os valores do formulário
    username=$(echo "$DADOS" | awk -F '|' '{print $1}')
    ip=$(echo "$DADOS" | awk -F '|' '{print $2}')
    password=$(echo "$DADOS" | awk -F '|' '{print $3}')
    destination_directory=$(echo "$DADOS" | awk -F '|' '{print $4}')
    source_file=$(echo "$DADOS" | awk -F '|' '{print $5}')
}

# Função para exibir os arquivos locais e remotos
function exibir_arquivos {
    local_files=$(ls | yad --title "Arquivos Locais" --list --column "Arquivos" --width=500 --height=600 --center)
    remote_files=$(sshpass -p "$password" ssh $username@$ip "ls -F" $destination_directory)
    remote_files=$(echo "$remote_files" | yad --title "Arquivos Remotos" --list --column "Arquivos" --center)
}

# Função para adicionar informações de conexão ao arquivo ".transfer_history"
function adicionar_historico {
    echo "Data e hora: $(date)" >> ~/.transfer_history
    echo "Arquivo transferido: $source_file" >> ~/.transfer_history
    echo "Servidor: $ip" >> ~/.transfer_history
    echo "Diretório de destino: $destination_directory" >> ~/.transfer_history
    echo "" >> ~/.transfer_history
}

# Função para exibir as informações de conexões anteriores
function exibir_historico {
    yad --title "Conexões Anteriores" --text "Informações sobre as conexões anteriores:" --text-align center --text-align right --center

    while read -r line; do
        yad --text "$line" --width=500 --height=200 --center
    done < ~/.transfer_history
}
