# Projeto da Disciplina Programação Script

## Descrição

Deve ser escrito um script que proveja algumas das funcionalidades do scp. Especificamente, a cópia de arquivos de e para uma outra máquina.

Seu escript deve fazer uso de modularização, isto é, deve utilizar diferentes scripts para cada função.

Para obter nota 50, seu script deve funcionar exibindo menus em modo de linha de comando.

Para obter nota 70, seu script deve possuir uma interface gráfica básica. (Pode usar o yad, por exemplo).

Para obter nota 100, seu script deve possuir interface gráfica e opções mais avançadas. A interface inicial deve assemelhar-se ao programa winscp. Deve exibir listas de arquivos na máquina cliente e na máquina servidor, e permitir a seleção e cópia de um ou de vários usando esta lista. Deve também relembrar dados de conexões anteriores

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jardson.silva/projeto-shellscript-scp_gui.git
git branch -M main
git push -uf origin main
```