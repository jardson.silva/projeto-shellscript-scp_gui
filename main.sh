#!/bin/bash

# Verifica se o arquivo ".transfer_history" existe, caso contrário, cria um
if [ ! -f ~/.transfer_history ]; then
    touch ~/.transfer_history
fi

# Importa as funções do arquivo "funcoes.sh"
source func.sh

# Chama a função para obter informações do usuário
obter_informacoes

# Exibe os arquivos locais e remotos
exibir_arquivos

# Chama a função para transferir o arquivo
echo "Transferindo arquivo para o servidor..."
sshpass -p "$password" scp $source_file $username@$ip:$destination_directory

# Chama a função para verificar a saída do comando "ssh"
ssh_error

# Chama a função para adicionar informações de conexão ao arquivo ".transfer_history"
adicionar_historico

echo "Transferência concluída."
yad --title "Concluído" --image gtk-dialog-info --text "A transferência do arquivo foi concluída com sucesso." --center

# Chama a função para exibir as informações de conexões anteriores
exibir_historico
